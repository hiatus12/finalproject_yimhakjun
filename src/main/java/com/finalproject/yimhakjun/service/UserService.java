package com.finalproject.yimhakjun.service;

import com.finalproject.yimhakjun.domain.dto.UserDto;
import com.finalproject.yimhakjun.domain.dto.UserJoinRequest;
import com.finalproject.yimhakjun.domain.dto.UserLoginRequest;
import com.finalproject.yimhakjun.domain.entity.User;
import com.finalproject.yimhakjun.exception.AppException;
import com.finalproject.yimhakjun.exception.ErrorCode;
import com.finalproject.yimhakjun.repository.UserRepository;
import com.finalproject.yimhakjun.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {


    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String key;
    private Long expireTimeMs= 1000*60*60L;

    public UserDto join(UserJoinRequest req) {

        userRepository.findByUserName(req.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, String.format("userName: %s", req.getUserName()));
                });

        String password = encoder.encode(req.getPassword());
        User savedUser = userRepository.save(req.toEntity(password));

        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .password(savedUser.getPassword())
                .build();
    }

    public String login(String userName,String password){
        //username없음
        User selectedUser = userRepository.findByUserName(userName)
                .orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND,userName+"이 없습니다."));

        //password틀림
        if(!encoder.matches(password,selectedUser.getPassword())){
            throw new AppException(ErrorCode.INVALID_PASSWORD, "패스워드를 잘못 입력하셨습니다.");
        }

        //앞에서 Exception안났으면 토큰 발행
        String token = JwtTokenUtil.createToken(selectedUser.getUserName(), key, expireTimeMs);
        return token;
    }

    public User getUserByUserName(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ""));
    }

}
