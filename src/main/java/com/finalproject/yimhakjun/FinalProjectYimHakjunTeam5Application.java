package com.finalproject.yimhakjun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectYimHakjunTeam5Application {

    public static void main(String[] args) {
        SpringApplication.run(FinalProjectYimHakjunTeam5Application.class, args);
    }

}
